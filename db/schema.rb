# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_22_182610) do

  create_table "accounts", force: :cascade do |t|
    t.integer "user_id"
    t.string "entity_type"
    t.string "account_name"
    t.string "address"
    t.float "balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ledgers", force: :cascade do |t|
    t.integer "account_id"
    t.string "direction"
    t.string "from"
    t.string "to"
    t.float "amount"
    t.string "txid"
    t.string "tx"
    t.string "popchannel_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pop_channels", force: :cascade do |t|
    t.integer "ledger_id"
    t.time "time"
    t.string "address"
    t.string "poptx"
    t.string "nonce"
    t.string "popchannel_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pop_sessions", force: :cascade do |t|
    t.integer "ledger_id"
    t.time "time"
    t.string "address"
    t.string "poptx"
    t.string "nonce"
    t.string "popchannel_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
