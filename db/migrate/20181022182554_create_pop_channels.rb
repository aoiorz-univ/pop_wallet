class CreatePopChannels < ActiveRecord::Migration[5.2]
  def change
    create_table :pop_channels do |t|
      t.integer :ledger_id
      t.time :time
      t.string :address
      t.string :poptx
      t.string :nonce
      t.string :popchannel_url

      t.timestamps
    end
  end
end
