class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.integer :user_id
      t.string :entity_type
      t.string :account_name
      t.string :address
      t.float :balance

      t.timestamps
    end
  end
end
