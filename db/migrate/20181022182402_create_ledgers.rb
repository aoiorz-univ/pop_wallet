class CreateLedgers < ActiveRecord::Migration[5.2]
  def change
    create_table :ledgers do |t|
      t.integer :account_id
      t.string :direction
      t.string :from
      t.string :to
      t.float :amount
      t.string :txid
      t.string :tx
      t.string :popchannel_url

      t.timestamps
    end
  end
end
