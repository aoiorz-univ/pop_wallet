class Ledger < ApplicationRecord
  belongs_to :account
  has_many :pop_sessions
  has_many :pop_channels
end
