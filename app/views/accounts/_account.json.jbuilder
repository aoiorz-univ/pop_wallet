json.extract! account, :id, :user_id, :entity_type, :account_name, :address, :balance, :created_at, :updated_at
json.url account_url(account, format: :json)
