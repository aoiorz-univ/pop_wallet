json.extract! pop_channel, :id, :ledger_id, :time, :address, :poptx, :nonce, :popchannel_url, :created_at, :updated_at
json.url pop_channel_url(pop_channel, format: :json)
