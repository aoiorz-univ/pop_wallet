json.extract! pop_session, :id, :ledger_id, :time, :address, :poptx, :nonce, :popchannel_url, :created_at, :updated_at
json.url pop_session_url(pop_session, format: :json)
