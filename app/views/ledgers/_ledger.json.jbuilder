json.extract! ledger, :id, :account_id, :direction, :from, :to, :amount, :txid, :tx, :popchannel_url, :created_at, :updated_at
json.url ledger_url(ledger, format: :json)
