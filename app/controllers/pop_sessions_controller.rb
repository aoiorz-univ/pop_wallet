class PopSessionsController < ApplicationController
  before_action :set_pop_session, only: [:show, :edit, :update, :destroy]

  # GET /pop_sessions
  # GET /pop_sessions.json
  def index
    @pop_sessions = PopSession.all
  end

  # GET /pop_sessions/1
  # GET /pop_sessions/1.json
  def show
  end

  # GET /pop_sessions/new
  def new
    @pop_session = PopSession.new
  end

  # GET /pop_sessions/1/edit
  def edit
  end

  # POST /pop_sessions
  # POST /pop_sessions.json
  def create
    @pop_session = PopSession.new(pop_session_params)

    respond_to do |format|
      if @pop_session.save
        format.html { redirect_to @pop_session, notice: 'Pop session was successfully created.' }
        format.json { render :show, status: :created, location: @pop_session }
      else
        format.html { render :new }
        format.json { render json: @pop_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pop_sessions/1
  # PATCH/PUT /pop_sessions/1.json
  def update
    respond_to do |format|
      if @pop_session.update(pop_session_params)
        format.html { redirect_to @pop_session, notice: 'Pop session was successfully updated.' }
        format.json { render :show, status: :ok, location: @pop_session }
      else
        format.html { render :edit }
        format.json { render json: @pop_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pop_sessions/1
  # DELETE /pop_sessions/1.json
  def destroy
    @pop_session.destroy
    respond_to do |format|
      format.html { redirect_to pop_sessions_url, notice: 'Pop session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pop_session
      @pop_session = PopSession.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pop_session_params
      params.require(:pop_session).permit(:ledger_id, :time, :address, :poptx, :nonce, :popchannel_url)
    end
end
