class PopChannelsController < ApplicationController
  before_action :set_pop_channel, only: [:show, :edit, :update, :destroy]

  # GET /pop_channels
  # GET /pop_channels.json
  def index
    @pop_channels = PopChannel.all
  end

  # GET /pop_channels/1
  # GET /pop_channels/1.json
  def show
  end

  # GET /pop_channels/new
  def new
    @pop_channel = PopChannel.new
  end

  # GET /pop_channels/1/edit
  def edit
  end

  # POST /pop_channels
  # POST /pop_channels.json
  def create
    @pop_channel = PopChannel.new(pop_channel_params)

    respond_to do |format|
      if @pop_channel.save
        format.html { redirect_to @pop_channel, notice: 'Pop channel was successfully created.' }
        format.json { render :show, status: :created, location: @pop_channel }
      else
        format.html { render :new }
        format.json { render json: @pop_channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pop_channels/1
  # PATCH/PUT /pop_channels/1.json
  def update
    respond_to do |format|
      if @pop_channel.update(pop_channel_params)
        format.html { redirect_to @pop_channel, notice: 'Pop channel was successfully updated.' }
        format.json { render :show, status: :ok, location: @pop_channel }
      else
        format.html { render :edit }
        format.json { render json: @pop_channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pop_channels/1
  # DELETE /pop_channels/1.json
  def destroy
    @pop_channel.destroy
    respond_to do |format|
      format.html { redirect_to pop_channels_url, notice: 'Pop channel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pop_channel
      @pop_channel = PopChannel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pop_channel_params
      params.require(:pop_channel).permit(:ledger_id, :time, :address, :poptx, :nonce, :popchannel_url)
    end
end
