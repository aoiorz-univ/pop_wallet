# coding: utf-8                                                                                                                
class LedgersController < ApplicationController
  before_action :set_Ledger, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /Ledgers                                                                                                                  
  # GET /Ledgers.json                                                                                                             
  def index
    @Ledgers = Ledger.all
  end

  # GET /Ledgers/1                                                                                                                
  # GET /Ledgers/1.json                                                                                                           
  def show
  end

  # GET /Ledgers/new                                                                                                              
  def new
    # 残高を調べる                                                                                                             
    @balance=bitcoinRPC('getreceivedbyaddress',[params[:send_address]])
    # ワレットIDを送付                                                                                                         
    @params=params
    @Ledger = Ledger.new
  end

  # GET /Ledgers/1/edit                                                                                                           
  def edit
  end

  # POST /Ledgers                                                                                                                 
  # POST /Ledgers.json                                                                                                            
  def create
    @Ledger = Ledger.new(Ledger_params)

    respond_to do |format|
      if @Ledger.save
        format.html { redirect_to @Ledger, notice: 'Ledger was successfully created.' }
        format.json { render :show, status: :created, location: @Ledger }
      else
        format.html { render :new }
        format.json { render json: @Ledger.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /Ledgers/1                                                                                                          
  # PATCH/PUT /Ledgers/1.json                                                                                                     
  def update
    respond_to do |format|
      if @Ledger.update(Ledger_params)
        format.html { redirect_to @Ledger, notice: 'Ledger was successfully updated.' }
        format.json { render :show, status: :ok, location: @Ledger }
      else
        format.html { render :edit }
        format.json { render json: @Ledger.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /Ledgers/1                                                                                                             
  # DELETE /Ledgers/1.json                                                                                                        
  def destroy
    @Ledger.destroy
    respond_to do |format|
      format.html { redirect_to Ledgers_url, notice: 'Ledger was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.                                                      
    def set_Ledger
      @Ledger = Ledger.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.                                       
    def Ledger_params
      params.require(:Ledger).permit(:Account_id, :send_address, :receive_address, :amoount, :fee, :balance)
    end
end
