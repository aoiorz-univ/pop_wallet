Rails.application.routes.draw do
  resources :pop_sessions
  resources :pop_channels
  resources :ledgers
  resources :accounts
  root to: 'accounts#index'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html  
end

