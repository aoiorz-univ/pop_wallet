require 'test_helper'

class PopSessionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pop_session = pop_sessions(:one)
  end

  test "should get index" do
    get pop_sessions_url
    assert_response :success
  end

  test "should get new" do
    get new_pop_session_url
    assert_response :success
  end

  test "should create pop_session" do
    assert_difference('PopSession.count') do
      post pop_sessions_url, params: { pop_session: { address: @pop_session.address, ledger_id: @pop_session.ledger_id, nonce: @pop_session.nonce, popchannel_url: @pop_session.popchannel_url, poptx: @pop_session.poptx, time: @pop_session.time } }
    end

    assert_redirected_to pop_session_url(PopSession.last)
  end

  test "should show pop_session" do
    get pop_session_url(@pop_session)
    assert_response :success
  end

  test "should get edit" do
    get edit_pop_session_url(@pop_session)
    assert_response :success
  end

  test "should update pop_session" do
    patch pop_session_url(@pop_session), params: { pop_session: { address: @pop_session.address, ledger_id: @pop_session.ledger_id, nonce: @pop_session.nonce, popchannel_url: @pop_session.popchannel_url, poptx: @pop_session.poptx, time: @pop_session.time } }
    assert_redirected_to pop_session_url(@pop_session)
  end

  test "should destroy pop_session" do
    assert_difference('PopSession.count', -1) do
      delete pop_session_url(@pop_session)
    end

    assert_redirected_to pop_sessions_url
  end
end
