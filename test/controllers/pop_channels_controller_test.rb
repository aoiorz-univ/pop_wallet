require 'test_helper'

class PopChannelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pop_channel = pop_channels(:one)
  end

  test "should get index" do
    get pop_channels_url
    assert_response :success
  end

  test "should get new" do
    get new_pop_channel_url
    assert_response :success
  end

  test "should create pop_channel" do
    assert_difference('PopChannel.count') do
      post pop_channels_url, params: { pop_channel: { address: @pop_channel.address, ledger_id: @pop_channel.ledger_id, nonce: @pop_channel.nonce, popchannel_url: @pop_channel.popchannel_url, poptx: @pop_channel.poptx, time: @pop_channel.time } }
    end

    assert_redirected_to pop_channel_url(PopChannel.last)
  end

  test "should show pop_channel" do
    get pop_channel_url(@pop_channel)
    assert_response :success
  end

  test "should get edit" do
    get edit_pop_channel_url(@pop_channel)
    assert_response :success
  end

  test "should update pop_channel" do
    patch pop_channel_url(@pop_channel), params: { pop_channel: { address: @pop_channel.address, ledger_id: @pop_channel.ledger_id, nonce: @pop_channel.nonce, popchannel_url: @pop_channel.popchannel_url, poptx: @pop_channel.poptx, time: @pop_channel.time } }
    assert_redirected_to pop_channel_url(@pop_channel)
  end

  test "should destroy pop_channel" do
    assert_difference('PopChannel.count', -1) do
      delete pop_channel_url(@pop_channel)
    end

    assert_redirected_to pop_channels_url
  end
end
