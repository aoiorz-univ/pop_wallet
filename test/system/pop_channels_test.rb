require "application_system_test_case"

class PopChannelsTest < ApplicationSystemTestCase
  setup do
    @pop_channel = pop_channels(:one)
  end

  test "visiting the index" do
    visit pop_channels_url
    assert_selector "h1", text: "Pop Channels"
  end

  test "creating a Pop channel" do
    visit pop_channels_url
    click_on "New Pop Channel"

    fill_in "Address", with: @pop_channel.address
    fill_in "Ledger", with: @pop_channel.ledger_id
    fill_in "Nonce", with: @pop_channel.nonce
    fill_in "Popchannel Url", with: @pop_channel.popchannel_url
    fill_in "Poptx", with: @pop_channel.poptx
    fill_in "Time", with: @pop_channel.time
    click_on "Create Pop channel"

    assert_text "Pop channel was successfully created"
    click_on "Back"
  end

  test "updating a Pop channel" do
    visit pop_channels_url
    click_on "Edit", match: :first

    fill_in "Address", with: @pop_channel.address
    fill_in "Ledger", with: @pop_channel.ledger_id
    fill_in "Nonce", with: @pop_channel.nonce
    fill_in "Popchannel Url", with: @pop_channel.popchannel_url
    fill_in "Poptx", with: @pop_channel.poptx
    fill_in "Time", with: @pop_channel.time
    click_on "Update Pop channel"

    assert_text "Pop channel was successfully updated"
    click_on "Back"
  end

  test "destroying a Pop channel" do
    visit pop_channels_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pop channel was successfully destroyed"
  end
end
