require "application_system_test_case"

class PopSessionsTest < ApplicationSystemTestCase
  setup do
    @pop_session = pop_sessions(:one)
  end

  test "visiting the index" do
    visit pop_sessions_url
    assert_selector "h1", text: "Pop Sessions"
  end

  test "creating a Pop session" do
    visit pop_sessions_url
    click_on "New Pop Session"

    fill_in "Address", with: @pop_session.address
    fill_in "Ledger", with: @pop_session.ledger_id
    fill_in "Nonce", with: @pop_session.nonce
    fill_in "Popchannel Url", with: @pop_session.popchannel_url
    fill_in "Poptx", with: @pop_session.poptx
    fill_in "Time", with: @pop_session.time
    click_on "Create Pop session"

    assert_text "Pop session was successfully created"
    click_on "Back"
  end

  test "updating a Pop session" do
    visit pop_sessions_url
    click_on "Edit", match: :first

    fill_in "Address", with: @pop_session.address
    fill_in "Ledger", with: @pop_session.ledger_id
    fill_in "Nonce", with: @pop_session.nonce
    fill_in "Popchannel Url", with: @pop_session.popchannel_url
    fill_in "Poptx", with: @pop_session.poptx
    fill_in "Time", with: @pop_session.time
    click_on "Update Pop session"

    assert_text "Pop session was successfully updated"
    click_on "Back"
  end

  test "destroying a Pop session" do
    visit pop_sessions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pop session was successfully destroyed"
  end
end
